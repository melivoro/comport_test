#ifndef DMXWORKER_H
#define DMXWORKER_H

#include <QSerialPort>
#include <QTimer>
#include <QThread>
#include <QDebug>

class DMXWorker : public QSerialPort
{
    Q_OBJECT
public:
    explicit DMXWorker(QString portName, QObject *parent = nullptr);

private slots:
    void onTimer();
    void onError(QSerialPort::SerialPortError error);

private:
    QTimer m_timer;
    QByteArray m_dmxArray;
};

#endif // DMXWORKER_H
