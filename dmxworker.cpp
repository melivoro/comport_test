#include "dmxworker.h"

DMXWorker::DMXWorker(QString portName, QObject *parent): QSerialPort(parent)
{
    connect(&m_timer, &QTimer::timeout, this, &DMXWorker::onTimer);
    connect(this, &QSerialPort::errorOccurred, this, &DMXWorker::onError);
    setPortName(portName);
    setBaudRate(250000, QSerialPort::Output);
    setDataBits(DataBits::Data8);
    setStopBits(StopBits::TwoStop);
    setParity(Parity::NoParity);
    setFlowControl(FlowControl::NoFlowControl);

    m_timer.setTimerType(Qt::PreciseTimer);
    m_timer.setInterval(30);
    m_timer.start();
    m_dmxArray.fill(0xFF, 5);
}

void DMXWorker::onTimer()
{
    if(!isOpen()) {
        bool result = open(QIODevice::ReadWrite);
        if(!result) {
            qDebug() << "DMXWorker::onTimer:" << tr("Error: %1 (port %2)").arg(errorString()).arg(portName());
            return;
        }
    }
    setBreakEnabled(true);
    QThread::usleep(110);
    setBreakEnabled(false);
    QThread::usleep(16);
    write(m_dmxArray);
}

void DMXWorker::onError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::WriteError) {
        close();
    }
}

